import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.JavaDirectoryService;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.search.GlobalSearchScope;
import exceptions.NoTestRootFound;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GeneratingTestTask {

    private static final String TEST_CLASS_EXTENSION = "Test";
    private final PsiClass sourceClass;
    private final VirtualFile sourceVirtualFile;
    private final Project project;

    public GeneratingTestTask(PsiClass sourceClass) {
        Objects.requireNonNull(sourceClass);
        this.sourceClass = sourceClass;
        this.sourceVirtualFile = sourceClass.getContainingFile().getVirtualFile();
        this.project = sourceClass.getProject();
    }

    private Optional<PsiClass> findClass(String fullyQualifiedName) {
        PsiClass testClass = JavaPsiFacade.getInstance(project)
                .findClass(fullyQualifiedName, GlobalSearchScope.allScope(project));
        return testClass != null ? Optional.of(testClass): Optional.empty();
    }

    /**
     * Adds the new test method to the existing or new test class
     * @param methodCode
     * @return
     */
    public PsiClass addTestMethod(String methodCode) {

       PsiClass[] testClassToReturn = new PsiClass[1];
        WriteCommandAction.runWriteCommandAction(project, () -> {

            String testClassName = sourceClass.getQualifiedName() + TEST_CLASS_EXTENSION;

            PsiClass testClass = getOrCreateTargetTestClass(testClassName);

            if (null == testClass) {
                return;
            }

            PsiMethod methodFromText = JavaPsiFacade.getElementFactory(project).createMethodFromText(methodCode, testClass);

            int noOfExistingMethods = testClass.getMethods().length;
            // add new test after last existing test method
            if (noOfExistingMethods > 0) {
                PsiMethod lastMethod = testClass.getMethods()[noOfExistingMethods - 1];
                testClass.addAfter(methodFromText, lastMethod);
            } else{
                testClass.add(methodFromText);
            }

            testClassToReturn[0] = testClass;
        });

        return testClassToReturn[0];
    }

    private PsiClass getOrCreateTargetTestClass(String qualifiedName) {
        return findClass(qualifiedName).orElseGet(() -> createTestClass(qualifiedName));
    }

    /**
     * Create the test class
     * @param testQualifiedName
     * @return
     */
    private PsiClass createTestClass(String testQualifiedName) {
        ProjectFileIndex fileIndex = ProjectRootManager.getInstance(project).getFileIndex();
        List<VirtualFile> allRoots = Arrays.asList(ProjectRootManager.getInstance(project).getContentSourceRoots());

        // find the closest test sources root
        VirtualFile testSourcesRoot = allRoots.stream().filter(fileIndex::isInTestSourceContent)
                .min(Comparator.comparingInt(a -> distance(a, sourceVirtualFile)))
                .orElseThrow(NoTestRootFound::new);

        // create all dirs necessary for new test class
        String[] testNameSplit = testQualifiedName.split("\\.");
        List<String> dirsToCreate = Stream.of(testNameSplit).collect(Collectors.toList());
        dirsToCreate.remove(dirsToCreate.size() - 1);
        VirtualFile testClassDir = createDirs(dirsToCreate, testSourcesRoot);

        PsiDirectory psiDirectory = PsiManager.getInstance(project).findDirectory(testClassDir);
        String testClassName = testNameSplit[testNameSplit.length-1];
        return JavaDirectoryService.getInstance().createClass(psiDirectory, testClassName);
    }

    /**
     * Create directories recursively
     * @param dirList the list of recursive directories to create
     * @param baseDir the base (relative) directory for the new directories
     * @return the last (most inner) directory
     */
    private VirtualFile createDirs(List<String> dirList, VirtualFile baseDir) {
        VirtualFile currentDir = baseDir;
        for (String dir : dirList) {
            Optional<VirtualFile> existingDir = Stream.of(currentDir.getChildren())
                    .filter(e -> e.getName().equals(dir)).findAny();

            VirtualFile finalCurrentDir = currentDir;
            currentDir = existingDir.orElseGet(()-> {
                try {
                    return finalCurrentDir.createChildDirectory(null, dir);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }
        return currentDir;
    }


    /**
     * Calculates the distance between 2 virtual files
     */
    private int distance(VirtualFile file1, VirtualFile file2) {
        Objects.requireNonNull(file1);
        Objects.requireNonNull(file2);

        HashMap<VirtualFile, Integer> pathToRoot = new HashMap<>();

        int dist = 0;
        do {
            pathToRoot.put(file1, dist++);
            file1 = file1.getParent();
        } while (file1 != null);

        dist = 0;
        do {
            if (pathToRoot.containsKey(file2)) {
                return pathToRoot.get(file2) + dist;
            }
            file2 = file2.getParent();
            dist++;
        } while (file2 != null);

        return Integer.MAX_VALUE;
    }
}
