import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.OpenFileDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiType;
import com.mock.gen.Generator;
import exceptions.NoTestRootFound;
import java.util.Arrays;
import java.util.stream.Collectors;
import org.jetbrains.annotations.NotNull;

public class UtAssistGenerate extends AnAction {


    private static final String TOOL_TIP = "ut-assist generate test";

    public UtAssistGenerate() {
        super(TOOL_TIP);
    }

    @Override
    public void actionPerformed(AnActionEvent event) {

        Project project = event.getProject();
        Editor editorData = event.getData(CommonDataKeys.EDITOR);
        if (editorData == null) {
            return;
        }

        PsiElement psiElement = event.getData(CommonDataKeys.PSI_ELEMENT);

        if (!(psiElement instanceof PsiMethod)){
            return;
        }

        PsiMethod psiMethod = (PsiMethod) psiElement;
        GeneratingTestTask generatingTestTask = new GeneratingTestTask(psiMethod.getContainingClass());
        String output = generateTestCode(editorData.getDocument().getText(), psiMethod);

        try {
            //add the new test
            PsiClass testClass = generatingTestTask.addTestMethod(output);
            navigateToNewMethod(project, testClass);
        } catch (NoTestRootFound e){
            noTestRootFoundMessageBox(project);
        }
    }

    private void navigateToNewMethod(Project project, PsiClass testClass) {
        VirtualFile testVirtualFile = testClass.getContainingFile().getVirtualFile();
        OpenFileDescriptor openFileDescriptor = new OpenFileDescriptor(project, testVirtualFile, Integer.MAX_VALUE,
                0);
        FileEditorManager fileEditorManager = FileEditorManager.getInstance(project);
        fileEditorManager.navigateToTextEditor(openFileDescriptor, true);
    }

    private void noTestRootFoundMessageBox(Project project) {
        Messages.showMessageDialog(project, "Please create a test root", "Test root not found",
                Messages.getInformationIcon());
    }

    private String generateTestCode(String sourceClassCode, PsiMethod sourceMethod) {
        // generate the test
        String methodSignature = getMethodSignature(sourceMethod);

        Generator generator = new Generator(methodSignature, sourceClassCode);
        return generator.generate();
    }

    @NotNull
    private String getMethodSignature(PsiMethod psiMethod) {
        String params = Arrays.stream(psiMethod.getParameterList().getParameters())
                .map(p -> p.getType().getPresentableText() + " " + p.getName()).collect(Collectors.joining(", "));

        PsiType returnType = psiMethod.getReturnType();
        String returnTypeString = returnType != null ? returnType.getPresentableText(): "";
        return psiMethod.getModifierList().getText() + " " +
                returnTypeString +" "+ psiMethod.getName() + " (" + params + ")";
    }
}